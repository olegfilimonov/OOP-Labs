﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listBox1.DataSource = Album.GetAlbums()
                .Select(al => al.Artist)
                .Distinct()
                .OrderBy(s => s)
                .ToList();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var curArtist = listBox1.Items[listBox1.SelectedIndex].ToString();
            listBox2.DataSource = Album.GetAlbums()
                .Where(al => al.Artist == curArtist)
                .OrderByDescending(al => al.Date)
                .ToList();
        }
    }
}
