﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OOP_Labs.lab3;

namespace TEST_OOP_Labs.lab3
{
    [TestClass()]
    public class WhatDayLab3Tests
    {
        private WhatDayLab3 testLab;

        public WhatDayLab3Tests()
        {
            testLab = new WhatDayLab3();
        }

        
        public void executeTest(int day, int dayNum, string monthName, int year = 1)
        {
            testLab.launch(day,year);
            Assert.AreEqual(dayNum, testLab.dayNum);
            Assert.AreEqual(monthName,testLab.monthName);
        }

        [TestMethod()]
        public void test1() => executeTest(32, 1, "February");
        [TestMethod()]
        public void test2() => executeTest(60, 1, "March");
        [TestMethod()]
        public void test3() => executeTest(91, 1, "April");
        [TestMethod()]
        public void test4() => executeTest(186, 5, "July");
        [TestMethod()]
        public void test5() => executeTest(304, 31, "October");
        [TestMethod()]
        public void test6() => executeTest(309, 5, "November");
        [TestMethod()]
        public void test7() => executeTest(327, 23, "November");
        [TestMethod()]
        public void test8() => executeTest(359, 25, "December");

        [TestMethod()]
        public void весокосныйТест() => executeTest(366, 31, "December",2004);

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void тестНаОшибку() => executeTest(367, 25, "December");

    }
}