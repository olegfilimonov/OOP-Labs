﻿using System;
using System.Collections.Generic;
using OOP_Labs.lab1;
using OOP_Labs.lab2;
using OOP_Labs.lab3;
using OOP_Labs.lab4;
using OOP_Labs.lab5;
using OOP_Labs.lab6;
using OOP_Labs.lab7;
using OOP_Labs.lab7.Bank;

namespace OOP_Labs
{
    class Launcher
    {
        private readonly List<OOP_Lab> _labs = new List<OOP_Lab>();

        private void AddLabs()
        {
            _labs.Add(new GreeterLab1()); // Лаба 1.1
            _labs.Add(new DivideItLab1()); // Лаба 1.2
            _labs.Add(new BankAccountLab2()); // Лаба 2.1
            _labs.Add(new StructTypeLab2()); // Лаба 2.2
            _labs.Add(new WhatDayLab3()); // Лаба 3
            _labs.Add(new Utils()); // Лаба 4
            var args = new[] { "..\\..\\lab5\\file.txt" };
            _labs.Add(new FileDetails(args)); // Лаба 5
            _labs.Add(new MatrixMultiply()); // Лаба 5
            _labs.Add(new CreateAccount()); // Лаба 6
            _labs.Add(new Bank()); // Лаба 7
        }

        public Launcher()
        {
            Console.Clear();

            AddLabs();

            Console.WriteLine("Выберите лабу, полный список:");
            var k = 0;
            foreach (var lab in _labs)
            {
                k++;
                Console.WriteLine("[{0}] Лабораторная №{1}: \"{2}\"", k, lab.Num, lab.Name);
            }
            var chosenValue = Convert.ToInt32(Console.ReadLine());
            
            
            

            Console.Clear();
            if (chosenValue > _labs.Count) throw new Exception("Такой лабы не существует");
            var currLab = _labs[chosenValue - 1];
            Console.WriteLine("Выполняется лабораторная № {0}: \"{1}\"", currLab.Num, currLab.Name);
            Console.WriteLine(("").PadRight(79, '='));

            try
            {
                currLab.execute();
            }
            catch (Exception ignored)
            {
                Console.WriteLine("Exception: " + ignored);
            }
            finally
            {
                Console.WriteLine("Конец работы, нажмите 'r' чтобы повторить, и любую клавишу для выхода");
                var chosen = Convert.ToChar(Console.ReadKey().KeyChar.ToString());
                if (chosen == 'r' || chosen == 'к') new Launcher();
            }
        }
    }

}
