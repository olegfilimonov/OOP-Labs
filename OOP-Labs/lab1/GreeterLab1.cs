﻿using System;

namespace OOP_Labs.lab1
{
    class GreeterLab1 : OOP_Lab
    {
        public override void execute()
        {
            string myName;
            Console.WriteLine("Please enter your name, guy");
            myName = Console.ReadLine();
            Console.WriteLine("Greetings, {0}", myName);
        }

        public GreeterLab1() : base("Обзор языка С# - Имя", 1)
        { }
    }
}
