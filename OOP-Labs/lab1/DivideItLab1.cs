﻿using System;

namespace OOP_Labs.lab1
{
    class DivideItLab1 : OOP_Lab
    {
        int i, j;
        string temp;

        public DivideItLab1() : base("Обзор языка С# - Деление", 1)
        {
        }

        public override void execute()
        {
            Console.WriteLine("Введите i");
            temp = Console.ReadLine();
            i = Convert.ToInt32(temp);

            Console.WriteLine("Введите j");
            temp = Console.ReadLine();
            j = Convert.ToInt32(temp);

            int k;

            try
            {
                k = i/j;
                Console.WriteLine("Ответ: {0}",k);
            }
            catch (Exception ignored)
            {
                Console.WriteLine(ignored.Message);
            }
        }
    }
}
