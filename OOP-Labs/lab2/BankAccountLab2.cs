﻿using System;

namespace OOP_Labs.lab2
{
    class BankAccountLab2 : OOP_Lab
    {
        AccountType goldAccount,platinumAccount;

        public BankAccountLab2() : base("Создание перечислимого типа", 2) {}

        public override void execute()
        {
            goldAccount = AccountType.Checking;
            platinumAccount = AccountType.Deposit;
            Console.WriteLine(goldAccount.ToString());
            Console.WriteLine(platinumAccount.ToString());
        }
    }
}
