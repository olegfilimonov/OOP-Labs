﻿using System;

namespace OOP_Labs.lab2
{

    class StructTypeLab2: OOP_Lab
    {
        private BankAccount goldAccount;

        public StructTypeLab2() : base("Создание типа структура",2)
        {
        }

        public struct BankAccount
        {
            public long accNo;
            public decimal accBal;
            public AccountType accType;

            public override string ToString()
            {
                return String.Format("Номер: {0}\nБаланс: {1}\nТип: {2}", accNo, accBal, accType.ToString());
            }
        }

        public override void execute()
        {
            goldAccount.accBal = int.MaxValue;
            goldAccount.accNo = 42;
            goldAccount.accType = AccountType.Deposit;
            Console.WriteLine(goldAccount.ToString());
        }
    }
}
