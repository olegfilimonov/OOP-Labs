﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Labs.lab5
{
    class FileDetails : OOP_Lab
    {
        private string[] args;
        private string filename;

        public FileDetails(string[] args) : base("Работа с массивом", 5)
        {
            this.args = args;
        }

        public override void execute()
        {
            Console.WriteLine("Длина массива аргументов: {0}", args.Length);
            var k = 0;
            foreach (var arg in args)
            {
                k++;
                Console.WriteLine("Аргумент №{0}: {1}", k, arg);
            }
            filename = args[0];
            try
            {
                var stream = new FileStream(filename, FileMode.Open);
                var reader = new StreamReader(stream);
                Console.WriteLine("Длина файла: {0}", stream.Length);
                var content = new char[stream.Length];
                content = reader.ReadToEnd().ToCharArray();
                summerize(content);
                reader.Close();
                stream.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine("Невозможно открыть файл. Ошибка: {0}",e.Message);
            }
        }

        private void summerize(char[] content)
        {
            int vowelCount = 0, convowelCount = 0;
            foreach (var curLetter in content)
            {
                if ("AEIOUaeiou".IndexOf(curLetter) != -1) vowelCount++;
                else convowelCount++;
            }
            Console.WriteLine("Количество гласных: {0}\nКоличество согласных: {1}", vowelCount, convowelCount);
        }
    }
}
