﻿using System;

namespace OOP_Labs.lab5
{
    class MatrixMultiply : OOP_Lab
    {
        public MatrixMultiply()
            : base("Использование методов со ссылочными параметрами", 5)
        {
        }

        public override void execute()
        {
            var arg1 = new double[2, 2];
            var arg2 = new double[2, 2];

            //Вводим данные
            Console.WriteLine("Введите 4 числа первый матрицы");
            var str = Console.ReadLine();
            for (int i = 0; i < 4; i++)
                arg1[i / 2, i % 2] = double.Parse(
                    (str = str.Substring((i == 0) ? 0 : str.IndexOf(' ') + 1))
                    .Substring(0, (str.IndexOf(' ') == -1 ? str.Length : str.IndexOf(' '))));

            Console.WriteLine("Введите 4 числа второй матрицы");
            str = Console.ReadLine();
            for (int i = 0; i < 4; i++)
                arg2[i / 2, i % 2] = double.Parse(
                    (str = str.Substring((i == 0) ? 0 : str.IndexOf(' ') + 1))
                        .Substring(0, (str.IndexOf(' ') == -1 ? str.Length : str.IndexOf(' '))));


            //Считаем результат
            var result = new double[2, 2];

            for (var i = 0; i < 2; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    result[i, j] = arg1[0, j] * arg2[0, j] + arg1[1, j] * arg2[1, j] + arg1[i, 0] * arg2[i, 0] + arg1[i, 1] * arg2[i, 1];
                }
            }

            //Выводим ответ
            Console.WriteLine("Ответ");
            Console.WriteLine("{0},{1}\n{2},{3}", result[0, 0], result[0, 1], result[1, 0], result[1, 1]);
        }
    }
}
