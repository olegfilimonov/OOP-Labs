﻿using System;
using OOP_Labs.lab7;
using OOP_Labs.lab7.Bank;
using AccountType = OOP_Labs.lab2.AccountType;

namespace OOP_Labs.lab6
{
    class CreateAccount : OOP_Lab
    {
        public CreateAccount() :
            base("Создание класс", 6)
        {
        }

        public void TestDeposit(BankAccount account)
        {
            Console.WriteLine("Enter amount to deposit: ");
            account.Deposit(decimal.Parse(s: Console.ReadLine()));
        }

        public void TestWithdraw(BankAccount account)
        {
            Console.WriteLine("Enter amount to withdraw: ");
            account.Withdraw(decimal.Parse(Console.ReadLine()));
        }

        public override void execute()
        { 
            BankAccount berts = NewBankAccount();
            Write(berts);

            BankAccount freds = NewBankAccount();
            Write(freds);
        
            TestDeposit(berts);
            Write(berts);

            TestWithdraw(berts);
            Write(berts);
        }

        BankAccount NewBankAccount()
        {
            BankAccount created = new BankAccount();


            created.AccNo = 123;

            //Console.Write("Enter the account number   : ");
            long number = BankAccount.NextNumber();

            Console.Write("Enter the account balance! : ");
            decimal balance = decimal.Parse(Console.ReadLine());

            created.AccBal = balance;
            created.AccType = (lab7.Bank.AccountType) AccountType.Checking;

            return created;
        }

        void Write(BankAccount account)
        {
            Console.WriteLine("Account number is {0}", account.AccNo);
            Console.WriteLine("Account balance is {0}", account.AccBal);
            Console.WriteLine("Account type is {0}", account.AccType);
        }
    }
}
