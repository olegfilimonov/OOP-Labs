﻿using System;
using System.Collections.Generic;
using OOP_Labs.lab4;
using OOP_Labs.lab5;

namespace OOP_Labs
{
    internal class Program
    {
        private static void Main(string[] args) => new Launcher();
    }

}
