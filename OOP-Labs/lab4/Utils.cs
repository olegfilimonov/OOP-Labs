﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Labs.lab4
{
    class Utils : OOP_Lab
    {
        public Utils() :
            base("Использование параметров в методах", 4)
        {
        }

        public static int Greater(int a, int b)
        {
            return (a > b) ? a : b;
        }

        public static void Swap(ref int a, ref int b)
        {
            var c = a;
            a = b;
            b = c;
        }

        public static bool Factorial(int n, out int ans)
        {
            try
            {

                ans = 1;
                for (var i = 1; i <= n; i++)
                {
                    ans *= i;
                }
                if (n > 25 || ans < 0) throw new OverflowException();
                return true;
            }
            catch (Exception e)
            {
                ans = int.MinValue;
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public static int RecursiveFactorial(int n)
        {
            if (n < 2)
                return 1;
            return (n)*RecursiveFactorial(n - 1);
        }

        public override void execute()
        {
            new Test();
        }
    }
}
