using System;

namespace OOP_Labs.lab4
{
    class Test
    {
        private int x, y, greater;

        public Test()
        {
            string tmp;
            Console.WriteLine("Enter x");
            tmp = Console.ReadLine();
            x = Convert.ToInt32(tmp);
            Console.WriteLine("Enter y");
            tmp = Console.ReadLine();
            y = Convert.ToInt32(tmp);

            //���� 4.2
            Console.WriteLine("Before swap: x={0},y={1}", x, y);
            Utils.Swap(ref x, ref y);
            Console.WriteLine("After swap: x={0},y={1}", x, y);

            //���� 4.1
            greater = Utils.Greater(x, y);
            Console.WriteLine("Greater: " + greater);

            //���� 4.3
            Console.WriteLine("Enter n for factorial");
            tmp = Console.ReadLine();
            var n = Convert.ToInt32(tmp);
            int ans;
            var success = Utils.Factorial(n, out ans);
            if (success) Console.WriteLine("Good. Answer: {0}", ans);
            else Console.WriteLine("Error");

            //���� 4.4
            ans = Utils.RecursiveFactorial(n);
            Console.WriteLine("Answer: {0}",ans);
        }
    }
}