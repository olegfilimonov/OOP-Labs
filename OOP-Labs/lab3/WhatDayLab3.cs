﻿using System;
using System.Collections;

namespace OOP_Labs.lab3
{
    public class WhatDayLab3 : OOP_Lab
    {
        public string monthName;
        public int dayNum;
        private bool isLeapYear;
        private int maxDayNum;

        public WhatDayLab3() : base("Конвертирование числа в пару месяц день", 3)
        {
        }

        public void launch(int day, int year = 1)
        {
            isLeapYear = year % 4 == 0;
            maxDayNum = (isLeapYear) ? 366 : 365;
            if ((day <= 0) || (day > maxDayNum)) throw new ArgumentOutOfRangeException("Day out of range");
            this.dayNum = day;
            int monthNum = 0;

            var collection = (isLeapYear) ? DaysInLeapMonths : DaysInMonths;

            foreach (int daysInMonth in collection)
            {
                if (dayNum <= daysInMonth)
                {
                    break;
                }
                else
                {
                    dayNum -= daysInMonth;
                    monthNum++;
                }
            }

            var name = (MonthName)monthNum;
            monthName = name.ToString();

            Console.WriteLine("{0},{1}", dayNum, monthName);
        }

        public override void execute()
        {
            Console.WriteLine("Please enter a year ");
            var line = Console.ReadLine();
            var year = int.Parse(line);
            isLeapYear = year % 4 == 0;
            maxDayNum = (isLeapYear) ? 366 : 365;

            Console.WriteLine("Please enter a day number between 1 and {0}: ",maxDayNum);
            line = Console.ReadLine();
            dayNum = int.Parse(line);

            

            try
            {
                launch(dayNum);
            }
            catch (Exception ignored)
            {
                Console.WriteLine(ignored.Message);
            }
        }

        static ICollection DaysInMonths
            = new int[12] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        static ICollection DaysInLeapMonths
            = new int[12] { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    }
}
