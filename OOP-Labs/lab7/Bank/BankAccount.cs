using System;

namespace OOP_Labs.lab7.Bank
{
    public class BankAccount
    {
        private static long _nextNumber = 123;
        
        public void TransferFrom(ref BankAccount accFrom, decimal amount)
        {
            if (accFrom.Withdraw(amount))
            {
                Deposit(amount);
            }
        }


        public void Populate(decimal balance)
        {
            AccNo = NextNumber();
            AccBal = balance;
            AccType = AccountType.Checking;
        }

        public bool Withdraw(decimal amount)
        {
            var sufficientFunds = AccBal >= amount;
            if (sufficientFunds)
            {
                AccBal -= amount;
            }
            return sufficientFunds;
        }

        public decimal Deposit(decimal amount)
        {
            AccBal += amount;
            return AccBal;
        }

        public long Number()
        {
            return AccNo;
        }

        public decimal Balance()
        {
            return AccBal;
        }

        public static long NextNumber()
        {
            return _nextNumber++;
        }
        
        public long AccNo { get; set; }

        public decimal AccBal { get; set; }

        public AccountType AccType { get; set; }

        public override string ToString()
        {
            return $"BANK ACCOUNT #{AccNo}, balance: {AccBal}$, type: {AccType}";
        }
    }
}