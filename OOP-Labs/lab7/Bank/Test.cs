using System;
using System.Runtime.InteropServices;

namespace OOP_Labs.lab7.Bank
{

    public class Test
    {
        public Test()
        {
            var b1 = new BankAccount {AccNo =1,AccBal = 100,AccType = AccountType.Deposit};
            var b2 = new BankAccount { AccNo = 2, AccBal = 100, AccType = AccountType.Deposit };
            Console.WriteLine("Before:\n{0}\n{1}",b1,b2);
            b1.TransferFrom(ref b2,10);
            Console.WriteLine("Before:\n{0}\n{1}", b1, b2);
        }


    }
}

