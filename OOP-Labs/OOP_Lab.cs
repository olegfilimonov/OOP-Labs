﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Labs
{
    /// <summary>
    /// Базовый класс для лабораторной работы
    /// </summary>
    public abstract class OOP_Lab
    {
        private string name = "Unnamed Method";
        private int num = 0;

        /// <summary>
        /// Основная функция выполнения
        /// </summary>
        public abstract void execute();

        /// <summary>
        /// Возвращат название лабораторной работы
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Возвращает номер лабораторной работы
        /// </summary>
        public int Num
        {
            get { return num; }
            set { num = value; }
        }

        /// <summary>
        /// Элемент лабораторной работы
        /// </summary>
        /// <param name="name">Название лабы</param>
        /// <param name="num">Номер лабы</param>
        public OOP_Lab(string name, int num)
        {
            this.name = name;
            this.num = num;
        }
    }
}
